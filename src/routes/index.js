import { Router } from 'express';

/* eslint-disable new-cap */
const routes = Router();
/* eslint-enable new-cap */

/**
 * GET home page
 */
routes.get('/', (req, res) => {
	res.render('index', {title: 'Desafio SoftExpert | API'});
});


export default routes;

