'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _PersonController = require('../controllers/PersonController');

var _PersonController2 = _interopRequireDefault(_PersonController);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint-disable new-cap */
const routes = (0, _express.Router)();
/* eslint-enable new-cap */

const controller = new _PersonController2.default();

/**
 * @route GET /persons/
 * @group Person
 * @param { integer } id.query
 * @param { string } first_name.query
 * @param { string } last_name.query
 * @param { integer } age.query
 * @returns { object } 200 - Return JSON results
 */
routes.get('/:id?', controller.list);

/**
 * @route POST /persons/
 * @group Person
 * @param { Person.model } body.body.required 
 * @returns { object } 200 - Return JSON with success mesage
 */
routes.post('/', controller.create);

/**
 * @route PUT /persons/{id}
 * @group Person
 * @param { integer } id.query.required 
 * @param { Person.model } body.body.required 
 * @returns { object } 200 - Return JSON result
 */
routes.put('/:id', controller.update);

/**
 * @route DELETE /persons/
 * @group Person
 * @param { integer } id.query.required 
 * @returns { object } 200 - Return JSON with success mesage
 */
routes.delete('/:id', controller.delete);

exports.default = routes;
//# sourceMappingURL=PersonRoute.js.map