#!/bin/sh

cd /app

touch .env

echo APP_NAME=${APP_NAME} >> .env
echo NODE_ENV=production >> .env
echo BASE_PATH=${BASE_PATH}
echo DATABASE_CONNECTION_STRING=postgres://${DB_USER}:${DB_PASS}@${DB_HOST}:${DB_PORT}/${DB_NAME} >> .env
echo SECRET=${SECRET_TOKEN} >> .env
echo PORT=${PORT_API} >> .env

NODE_ENV=production npm run start; 